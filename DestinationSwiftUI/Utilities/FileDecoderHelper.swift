//
//  FileDecoderHelper.swift
//  DestinationSwiftUI
//
//  Created by rafiul hasan on 15/12/21.
//

import Foundation

struct FileDecoderHelper<T: Decodable> {
    private var fileName: String
    
    init(file name: String){
        fileName = name
    }
     
    func getData() -> [T] {
        guard let json = Bundle.main.url(forResource: fileName, withExtension: "json") else { fatalError("Json loading failed")}

        do {
            let jsonData = try Data(contentsOf: json)
            return try JSONDecoder().decode([T].self, from: jsonData)
        } catch let error{
            fatalError("Json parsing failed \(error)")
        }
    }
    
//    let destination: [Destination] = {
//        guard let json = Bundle.main.url(forResource: "destinations", withExtension: "json") else { fatalError("Json loading failed")}
//
//        do {
//            let jsonData = try Data(contentsOf: json)
//            return try JSONDecoder().decode([Destination].self, from: jsonData)
//        } catch let error{
//            fatalError("Json parsing failed \(error)")
//        }
//    }()
}
