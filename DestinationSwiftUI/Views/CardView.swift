//
//  CardView.swift
//  DestinationSwiftUI
//
//  Created by rafiul hasan on 15/12/21.
//

import SwiftUI
import MapKit

struct CardView: View {
    @Binding var destination: Destination
    @Binding var expand: Bool
    @State private var showMap = false
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 41.89040186262684, longitude: 12.492649323336162), span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
    
    var body: some View {
        ZStack(alignment: .topTrailing) {
            VStack {
                Image(destination.image)
                    .resizable()
                    .frame(height: destination.expand ? 300 : 250)
                    .cornerRadius(destination.expand ? 0 : 25)
                
                if destination.expand {
                    ScrollView {
                        HStack {
                            Text(destination.name)
                                .font(.title)
                                .bold()
                            Spacer()
                        }.padding()
                        
                        Text(destination.details)
                            .padding(.horizontal)
                        
                        if destination.sevenWonder {
                            HStack {
                                Image("sevenWonder")
                                    .resizable()
                                    .frame(width: 100, height: 100)
                                Text("This destination is one of the Seven wonder of the world!")
                            }
                        }
                        
                        
                        Map(coordinateRegion: .constant(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: Double(destination.latitude)!, longitude: Double(destination.longitude)!), span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))), interactionModes: [.zoom], showsUserLocation: false, userTrackingMode: .constant(.none), annotationItems: [destination.city], annotationContent: {
                            MapPin(coordinate: $0.coord, tint: .red)
                        })
                            .frame(height: 250)
                            .cornerRadius(25)
                            .padding()
                            .shadow(color: Color.black, radius: 5, x: 5, y: 5)
                            .shadow(color: Color.white, radius: 5, x: -5, y: -5)
                        
                        Button(action: {
                            showMap.toggle()
                        }) {
                            Text("Launch Map")
                        }.sheet(isPresented: $showMap) {
                            MapView(destination: destination)
                        }
                        .padding(.bottom)
                    }
                }
                Spacer(minLength: 0)
            }
            .padding(.horizontal, destination.expand ? 0 : 20)
            .contentShape(Rectangle())
            
            if destination.expand {
                Button(action: {
                    withAnimation(.interactiveSpring(response: 0.5, dampingFraction: 0.8, blendDuration: 0 )) {
                        destination.expand.toggle()
                        expand.toggle()
                    }
                }) {
                    Image(systemName: "xmark")
                        .foregroundColor(.blue)
                        .padding()
                        .background(Color.black.opacity(0.8))
                        .clipShape(Circle())
                }
                .padding(.top, UIApplication.shared.windows.first?.safeAreaInsets.top)
                .padding(.trailing, 10)
            }
        }
    }
}

//struct CardView_Previews: PreviewProvider {
//    static var previews: some View {
//        CardView()
//    }
//}
