//
//  HomeView.swift
//  DestinationSwiftUI
//
//  Created by rafiul hasan on 15/12/21.
//

import SwiftUI

struct HomeView: View {
    @State private var destination = FileDecoderHelper<Destination>(file: "destinations").getData()
    @State private var expandView: Bool = false
    
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    headerView
                    destinationsView
                }
            }
        }
    }
    
    private var headerView: some View {
        HStack {
            VStack {
                Text("Top Destinations")
                    .font(.largeTitle)
                    .bold()
            }
            Spacer()
        }.padding()
    }
    
    private var destinationsView: some View {
        VStack(spacing: 15) {
            ForEach(0..<destination.count) { index in
                GeometryReader { geo in
                    CardView(destination: $destination[index], expand: $expandView)
                        .padding(.horizontal, destination[index].expand ? 0 : 15)
                        .offset(y: destination[index].expand ? -geo.frame(in: .global).minY : 0)
                        .opacity(expandView ? (destination[index].expand ? 1 : 0) : 1)
                        .onTapGesture {
                            withAnimation(.interactiveSpring(response: 0.5, dampingFraction: 0.8, blendDuration: 0 )) {
                                if !destination[index].expand {
                                    expandView.toggle()
                                    destination[index].expand.toggle()
                                }
                            }
                        }
                }
                .frame(height: destination[index].expand ? UIScreen.main.bounds.height : 250)
                //.simultaneousGesture(DragGesture(minimumDistance: destination[index].expand ? 0 : 500))
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
