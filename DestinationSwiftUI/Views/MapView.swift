//
//  MapView.swift
//  DestinationSwiftUI
//
//  Created by rafiul hasan on 20/12/21.
//

import SwiftUI
import MapKit

struct MapView: View {
    @Environment(\.presentationMode) var presentationMode
    var destination: Destination
    
    var body: some View {
        ZStack(alignment: .topTrailing) {
            
            Map(coordinateRegion: .constant(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: Double(destination.latitude)!, longitude: Double(destination.longitude)!), span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))), interactionModes: [.zoom], showsUserLocation: false, userTrackingMode: .constant(.none), annotationItems: [destination.city], annotationContent: {
                MapPin(coordinate: $0.coord, tint: .red)
            })
            
            Button(action: {
                withAnimation(.interactiveSpring(response: 0.5, dampingFraction: 0.8, blendDuration: 0 )) {
                    presentationMode.wrappedValue.dismiss()
                }
            }) {
                Image(systemName: "xmark")
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.black.opacity(0.8))
                    .clipShape(Circle())
            }
            .padding(.top, 10)
            .padding(.trailing, 10)
            
        }
    }
}

//struct MapView_Previews: PreviewProvider {
//    static var previews: some View {
//        MapView(destination: Destination())
//    }
//}
