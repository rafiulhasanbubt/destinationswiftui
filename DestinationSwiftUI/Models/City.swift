//
//  City.swift
//  DestinationSwiftUI
//
//  Created by rafiul hasan on 15/12/21.
//

import Foundation
import CoreLocation

struct City: Identifiable {
    let id = UUID()
    let name: String
    let coord: CLLocationCoordinate2D
}

