//
//  Destination.swift
//  DestinationSwiftUI
//
//  Created by rafiul hasan on 15/12/21.
//

import Foundation
import CoreLocation
import SwiftUI

final class Destination: Decodable, Identifiable {
    let id = UUID()
    var name: String
    var latitude: String
    var longitude: String
    var image: String
    var sevenWonder: Bool
    var overlay: Bool
    var expand: Bool
    var details: String
    var city: City
    
    enum CodingKeys: String, CodingKey {
        case name
        case latitude
        case longitude
        case image
        case sevenWonder
        case overlay
        case expand
        case details
        case city
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        latitude = try values.decode(String.self, forKey: .latitude)
        longitude = try values.decode(String.self, forKey: .longitude)
        image = try values.decode(String.self, forKey: .image)
        sevenWonder = try values.decode(Bool.self, forKey: .sevenWonder)
        overlay = try values.decode(Bool.self, forKey: .overlay)
        expand = try values.decode(Bool.self, forKey: .expand)
        details = try values.decode(String.self, forKey: .details)
        
        city = City(name: name, coord: CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!))
    }
}
