//
//  ContentView.swift
//  DestinationSwiftUI
//
//  Created by rafiul hasan on 15/12/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
