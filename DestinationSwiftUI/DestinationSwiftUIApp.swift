//
//  DestinationSwiftUIApp.swift
//  DestinationSwiftUI
//
//  Created by rafiul hasan on 15/12/21.
//

import SwiftUI

@main
struct DestinationSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
